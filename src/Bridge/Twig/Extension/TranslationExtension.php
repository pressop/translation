<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translation\Bridge\Twig\Extension;

use Pressop\Component\Translation\ChainTranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class TranslatorExtension
 *
 * @author Benjamin Georgeault
 */
class TranslationExtension extends AbstractExtension
{
    /**
     * @var ChainTranslatorInterface
     */
    private $translator;

    /**
     * TranslatorExtension constructor.
     * @param ChainTranslatorInterface $translator
     */
    public function __construct(ChainTranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function getFilters()
    {
        return [
            new TwigFilter('ctrans', [$this->translator, 'trans']),
            new TwigFilter('ctranschoice', [$this->translator, 'transChoice']),
        ];
    }
}
