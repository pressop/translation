<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translation\Bridge\Symfony\Cache;

use Pressop\Component\Translation\ChainTranslator;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class CachedChainTranslator
 *
 * @author Benjamin Georgeault
 */
class CachedChainTranslator extends ChainTranslator
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param CacheInterface $cache
     * @return self
     */
    public function setCache(CacheInterface $cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function findTranslation(array $normalizedChain, string $locale = null, $default = true)
    {
        $key = $this->getKey($normalizedChain, $locale, $default);
        try {
            if (!$this->cache->has($key)) {
                $this->cache->set(
                    $key,
                    parent::findTranslation($normalizedChain, $locale, $default)
                );
            }

            return $this->cache->get($key);
        } catch (InvalidArgumentException $e) {
        }

        return parent::findTranslation($normalizedChain, $locale, $default);
    }

    /**
     * @param array $normalizedChain
     * @param string|null $locale
     * @param bool $default
     * @return string
     */
    private function getKey(array $normalizedChain, string $locale = null, $default = true): string
    {
        return json_encode($normalizedChain)
            . ($locale ? : '')
            . (is_string($default) ? $default : ($default ? 'true' : 'false'))
        ;
    }
}
