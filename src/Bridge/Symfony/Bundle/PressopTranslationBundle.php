<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translation\Bridge\Symfony\Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class PressopTranslationBundle
 *
 * @author Benjamin Georgeault
 */
class PressopTranslationBundle extends Bundle
{
}
