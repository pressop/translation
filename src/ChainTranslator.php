<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translation;

use Symfony\Component\Translation\TranslatorBagInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ChainTranslator
 *
 * @author Benjamin Georgeault
 */
class ChainTranslator implements ChainTranslatorInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Translator constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        if (!($translator instanceof TranslatorBagInterface)) {
            throw new \InvalidArgumentException(sprintf(
                'The translator has to implement "%s" too.',
                TranslatorBagInterface::class
            ));
        }

        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function trans(
        array $chain,
        array $parameters = [],
        string $defaultDomain = null,
        string $locale = null,
        $default = true
    ): ?string {
        if (0 === count($chain)) {
            throw new \InvalidArgumentException('The first argument cannot be empty.');
        }

        if (null !== $translation = $this->findTranslation($this->normalize($chain, $defaultDomain), $locale)) {
            if (is_string($translation)) {
                return $translation;
            }

            list ($id, $domain) = $translation;
            return $this->translator->trans($id, $parameters, $domain, $locale);
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function transChoice(
        array $chain,
        $number,
        array $parameters = [],
        string $defaultDomain = null,
        string $locale = null,
        $default = true
    ): ?string {
        if (0 === count($chain)) {
            throw new \InvalidArgumentException('The first argument cannot be empty.');
        }

        if (null !== $translation = $this->findTranslation($this->normalize($chain, $defaultDomain), $locale)) {
            list ($id, $domain) = $translation;
            return $this->translator->transChoice($id, $number, $parameters, $domain, $locale);
        }

        return null;
    }

    /**
     * @param array $normalizedChain
     * @param string|null $locale
     * @param bool $default
     * @return array|string|null
     */
    protected function findTranslation(array $normalizedChain, string $locale = null, $default = true)
    {
        $catalogue = $this->translator->getCatalogue($locale);

        foreach ($normalizedChain as $id => $domain) {
            if ($catalogue->has($id, $domain)) {
                return [$id, $domain];
            }
        }

        if (true === $default) {
            reset($normalizedChain);
            return [key($normalizedChain), current($normalizedChain)];
        } elseif (is_string($default)) {
            return [$default, 'messages'];
        }

        return null;
    }

    /**
     * @param array $chain
     * @param string|null $defaultDomain
     * @return array
     */
    private function normalize(array $chain, string $defaultDomain = null): array
    {
        $normalize = [];

        foreach ($chain as $id => $domain) {
            if (is_int($id)) {
                $id = $domain;
                $domain = $defaultDomain;
            }

            $normalize[$id] = $domain;
        }

        return $normalize;
    }
}
