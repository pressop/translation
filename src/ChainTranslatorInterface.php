<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translation;

/**
 * Interface ChainTranslatorInterface
 *
 * @author Benjamin Georgeault
 */
interface ChainTranslatorInterface
{
    /**
     * @param array $chain
     * @param array $parameters
     * @param string|null $defaultDomain
     * @param string|null $locale
     * @param string|boolean $default
     * @return string|null
     */
    public function trans(
        array $chain,
        array $parameters = array(),
        string $defaultDomain = null,
        string $locale = null,
        $default = true
    ): ?string;

    /**
     * @param array $chain
     * @param $number
     * @param array $parameters
     * @param string|null $defaultDomain
     * @param string|null $locale
     * @param string|boolean $default
     * @return string|null
     */
    public function transChoice(
        array $chain,
        $number,
        array $parameters = array(),
        string $defaultDomain = null,
        string $locale = null,
        $default = true
    ): ?string;
}
